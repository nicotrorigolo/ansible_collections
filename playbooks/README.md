# ansible_collections - playbooks

Chacun des dossiers est un playbook, un dossier contient  
playbook  
requierement  

Le playbook porte le nom du dossier
Le requierement porte le nom suivant : ansible_role_requierement.yaml  

## Récuperer les requierements

Pour téléchargers les roles qui sont dans le fichier de requierement, il faut executer la commande:  
ansible-galaxy install -r {chemin_vers_fichier}/ansible_role_requierement.yaml  

## Executer le playbook

Executer le playbook une fois que les requierements ont été téléchargés.  
Pour éxécuter le playbook exécuter la commande suivante:  
ansible-playbook -i {chemin_vers_fichier}/inventory.py {chemin_vers_fichier}/{nom_playbook}.yaml  

## Exemple d'utilisation

EN TANT QUE, utilisateur présent dans le dossier "ansible_collections"  
JE VEUX, executer le playbook présent dans le dossier "ajout_user_avec_ssh"  
ALORS, je télécharge les requierement du dossier en faisant :  
    ansible-galaxy install -r playbooks/ajout_user_avec_ssh/ansible_role_requierement.yaml  
ET j'execute le playbook du dossier en faisant :  
    ansible-playbook -i serveurs/terraform/inventory.py playbooks/ajout_user_avec_ssh/ajout_user_avec_ssh.yaml  
AFIN DE, executer le playbook "ajout_user_avec_ssh.yaml"

